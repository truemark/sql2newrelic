#!/usr/bin/env bash

# Change working directory to where the script is located
DIR=$(dirname ${0})
cd ${DIR}

set -uex

install_pips() {
    python3 -m pip install --upgrade pip
    pip install --upgrade requests
    pip install --upgrade pyyaml
    pip install --upgrade cx_Oracle
    pip install --upgrade pyodbc
}

create_venv() {
    if [ ! -d "python-venv" ]; then
        python3 -m venv python-venv
        source python-venv/bin/activate
        install_pips
    else
        source python-venv/bin/activate
    fi
}

create_venv