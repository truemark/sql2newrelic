# sql2newrelic

This utility runs a SQL query and sends the results to New Relic and exits. 
It is intended to be run from cron or other scheduler.

## Pre-requisites

This project assumes you have Python 3.6 and Python venv installed globally.

### 1. Oracle Client Install
If you are going to use sql2newrelic to talk to an Oracle database you need
to install the Oracle instant client.

1. Download the Oracle instant client from https://www.oracle.com/database/technologies/instant-client/downloads.html
2. Unpack the zip file and put the instantclient_18_1 folder into this project.


### MSSQL Driver Install

The following was used to get the MSSQL driver working on MacOS using Mac Ports

```bash
sudo port install unixodbc
sudo port install msodbcsql
sudo port install freetds
sudo mkdir -p /usr/local/opt/unixodbc/
sudo ln -s /opt/local/lib /usr/local/opt/unixodbc/lib

```

### 2. Python Virtual Env Setup

Execute ```./setup.sh```. This will create a python-venv directory that contains
all the dependencies used by this utility. 

## Usage

```bash
./wrapper.sh config.yml
```

## Related Links

 * https://cx-oracle.readthedocs.io/en/latest/
 * https://oracle.github.io/python-cx_Oracle/
 * https://www.oracle.com/technetwork/articles/dsl/python-091105.html