#!/usr/bin/env python3

import cx_Oracle, gzip, json, pyodbc, requests, socket, sys, time, yaml


def current_milli_time():
    return int(round(time.time() * 1000))


def get_oracle_connection(dsn: str):
    return cx_Oracle.connect(dsn)


def get_mssql_connection(dsn: str):
    return pyodbc.connect(dsn)


def execute(dbtype: str, dsn: str, query: str, results_record: dict):
    if dbtype == "mssql":
        con = get_mssql_connection(dsn)
    else:
        con = get_oracle_connection(dsn)
    try:
        cur = con.cursor()
        try:
            cur.execute(query)
            for row in cur:
                for val, desc in zip(row, cur.description):
                    results_record[desc[0]] = val
        finally:
            cur.close()
    finally:
        con.close()


def post(account: str, api_key, results: list):
    data = bytes(json.dumps(results), 'utf-8')
    payload = gzip.compress(data)
    url = "https://insights-collector.newrelic.com/v1/accounts/" + account + "/events"
    headers = {
        "Content-Type": "application/json",
        "X-Insert-Key": api_key,
        "Content-Encoding": "gzip"
    }
    r = requests.post(url, headers=headers, data=payload)
    return r.status_code == 200


def usage():
    print("usage: %s config" % (sys.argv[0]))


def main():

    if len(sys.argv) != 2:
        print("missing config")
        usage()
        sys.exit(1)

    with open(sys.argv[1], 'r') as stream:
        try:
            config = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print("failed to parse yml %s" % (sys.argv[1]))
            print(exc)
            sys.exit(1)

    if "mssql" in config.keys():
        dbconfig = config["mssql"]
        dbtype = "mssql"

    else:
        dbconfig = config["oracle"]
        dbtype = "oracle"

    dsn = dbconfig["dsn"]
    query = dbconfig["query"]
    newrelic = config["newrelic"]
    newrelic_account = newrelic["account"]
    newrelic_api_key = newrelic["api_key"]
    event_type = newrelic["event_type"]

    results_record = {
        "eventType": event_type,
        "executingHost": socket.gethostname(),
        "executingIp": socket.gethostbyname(socket.gethostname())
    }
    results = [results_record]

    start_time = current_milli_time()
    execute(dbtype, dsn, query, results_record)
    time_diff = current_milli_time() - start_time
    results_record["executionMilliseconds"] = time_diff

    success = post(newrelic_account, newrelic_api_key, results)
    if success:
        print("data successfully posted to newrelic")
    else:
        print("data failed to post to newrelic")


if __name__ == '__main__':
    main()
