FROM python:3.6

RUN curl https://packages.microsoft.com/keys/microsoft.asc | APT_KEY_DONT_WARN_ON_DANGEROUS_USAGE=DontWarn apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update
RUN ACCEPT_EULA=Y apt-get install msodbcsql17
RUN ACCEPT_EULA=Y apt-get install mssql-tools
RUN apt-get install unixodbc-dev

# Add user and setup home directory
ENV HOME=/home/user
RUN mkdir -p $HOME
RUN groupadd -r user &&\
    useradd -r -g user -G staff -d /home/user -s /bin/bash -c "Docker image user" user
RUN chown -R user:user $HOME

# Get Oracle Instant Client
ARG CURL_INSTSANTCLIENT_18_1
RUN ${CURL_INSTSANTCLIENT_18_1}
RUN unzip instantclient.zip -d /home/user

# Setup venv
RUN mkdir $HOME/app/
COPY setup.sh $HOME/app/
RUN chown -R user:user $HOME/app/
RUN su - user -c "$HOME/app/setup.sh"

COPY *.py $HOME/app/
COPY newrelic $HOME/app/
RUN chown -R user:user $HOME/app/
USER user