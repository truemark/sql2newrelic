#!/usr/bin/env bash

CWD=$(pwd)
DIR=$(dirname ${0})
cd ${DIR}

export LD_LIBRARY_PATH="./instantclient_18_1"
source ./python-venv/bin/activate
./python-venv/bin/python3 sql2newrelic.py ${CWD}/$1
